package ru.t1.sukhorukova.tm.exception.field;

public final class IdEmptyException extends AbstractFieldExceprion {

    public IdEmptyException() {
        super("Error! Id is empty...");
    }

}
