package ru.t1.sukhorukova.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sukhorukova.tm.model.AbstractUserOwnerModel;
import ru.t1.sukhorukova.tm.api.repository.IUserOwnerRepository;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public abstract class AbstractUserOwnerRepository<M extends AbstractUserOwnerModel> extends AbstractRepository<M> implements IUserOwnerRepository<M> {

    @Nullable @Override
    public M add(
            @Nullable final String userId,
            @NotNull final M model
    ) {
        if (userId == null) return null;
        model.setUserId(userId);
        return add(model);
    }

    @NotNull @Override
    public List<M> findAll(@NotNull final String userId) {
        return records.stream()
                .filter(m -> userId.equals(m.getUserId()))
                .collect(Collectors.toList());
    }

    @NotNull @Override
    public List<M> findAll(
            @NotNull final String userId,
            @NotNull final Comparator comparator
    ) {
        @NotNull final List<M> result = findAll(userId);
        result.sort(comparator);
        return result;
    }

    @Nullable @Override
    public M findOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (userId == null || id == null) return null;
        return records.stream()
                .filter(m -> userId.equals(m.getUserId()))
                .filter((m -> id.equals(m.getId())))
                .findFirst().orElse(null);
    }

    @Nullable @Override
    public M findOneByIndex(
            @Nullable final String userId,
            @Nullable final Integer index
    ) {
        if (userId == null || index == null) return null;
        return records.stream()
                .filter(m -> userId.equals(m.getUserId()))
                .skip(index)
                .findFirst().orElse(null);
    }

    @Nullable @Override
    public M removeOne(
            @Nullable final String userId,
            @Nullable final M model
    ) {
        if (userId == null || model == null) return null;
        return removeOneById(userId, model.getId());
    }

    @Nullable @Override
    public M removeOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (userId == null || id == null) return null;
        @Nullable final M model = findOneById(userId, id);
        if (model == null) return null;
        return removeOne(model);
    }

    @Nullable @Override
    public M removeOneByIndex(
            @Nullable final String userId,
            @Nullable final Integer index
    ) {
        if (userId == null || index == null) return null;
        @Nullable final M model = findOneByIndex(userId, index);
        if (model == null) return null;
        return removeOne(model);
    }

    @Override
    public void removeAll(@NotNull final String userId) {
        @NotNull final List<M> models = findAll(userId);
        for (final M model: models) removeOne(userId, model);
    }

    @Override
    public int getSize(@NotNull final String userId) {
        return (int) records.stream()
                .filter(m->userId.equals(m.getUserId()))
                .count();
    }

    @Override
    public boolean existsById(
            @Nullable final String userId,
            @Nullable String id
    ) {
        return findOneById(userId, id) != null;
    }

}
